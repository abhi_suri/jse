package com.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Employee;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class SpringMain {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/config/springconfig.xml");
		Employee employee = (Employee) applicationContext.getBean("employeeId");
		System.out.println("Spring" + employee);
		employee.setEmpNo(10);
		employee.setEmpName("Ajay");
		employee.setSalary(123.45f);
		System.out.println("Employee number : " + employee.getEmpNo());
		System.out.println("Employee name : " + employee.getEmpName());
		System.out.println("Employee salary : " + employee.getSalary());

		Employee employee2 = new Employee();
		System.out.println("Java" + employee2);
		System.out.println("Employee number : " + employee2.getEmpNo());
		System.out.println("Employee name : " + employee2.getEmpName());

		System.out.println("The End");

	}

}
