package com.service;

import java.util.List;

import com.exception.EmployeeException;
import com.model.Employee;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public interface EmployeeService {

	public abstract Employee createEmployee(Employee employee);

	public abstract List<Employee> readAllEmployee();

	public abstract Employee readValidateEmployeeById(int id) throws EmployeeException;

	public abstract Employee readEmployeeByName(String name);

	public abstract Employee updateEmployee(Employee employee);

	public abstract int deleteEmployeeById(int id);

	public abstract Employee deleteEmployeeName(String name);

}
