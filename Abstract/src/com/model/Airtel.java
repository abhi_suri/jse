package com.model;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class Airtel implements Recharge, DataRecharge {

	@Override
	public int Rs50() {
		System.out.println("50 SMS free - Airtel");
		System.out.println("Tax 13% deduct - Airtel");
		return 30;
	}

	@Override
	public int Rs100() {
		System.out.println("100 SMS free - Airtel");
		System.out.println("Tax 18% deduct - Airtel");
		return 80;
	}

	@Override
	public String dataFor50() {
		return "Internet for 5 days - Airtel";
	}

	@Override
	public String dataFor100() {
		return "Internet for 12 days - Airtel";
	}

}
