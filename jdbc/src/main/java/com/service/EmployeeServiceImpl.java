package com.service;

import java.util.List;

import com.exception.EmployeeException;
import com.model.Employee;

import dao.EmployeeDao;
import dao.EmployeeDaoImpl;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class EmployeeServiceImpl implements EmployeeService {

	@Override
	public Employee createEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Employee> readAllEmployee() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employee readValidateEmployeeById(int id) throws EmployeeException {

		Employee employeeDaoData = null;

		int length = String.valueOf(id).length();
		if (length > 1 && length < 5) {

			EmployeeDao employeeDao = new EmployeeDaoImpl();
			employeeDaoData = employeeDao.readEmployeeById(id);

		} else {

			throw new EmployeeException("Invalid user id ");
		}
		return employeeDaoData;
	}

	@Override
	public Employee readEmployeeByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employee updateEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int deleteEmployeeById(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Employee deleteEmployeeName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

}
