package com.model;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public abstract class Employee {
	public abstract float calculateSalary();

}
