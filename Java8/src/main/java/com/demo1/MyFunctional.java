package com.demo1;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

@FunctionalInterface
public interface MyFunctional {
	public abstract void display();

}
