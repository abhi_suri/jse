package com.model;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public interface Recharge {
	public static final int MINAMOUNT = 5;

	public abstract int Rs50();

	public abstract int Rs100();

}
