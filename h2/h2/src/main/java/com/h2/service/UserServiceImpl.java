package com.h2.service;

import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.h2.model.User;
import com.h2.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

@Service
@Slf4j
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository userRepository;
	private static Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	@Override
	@Transactional
	public User createUser(User user) {
		logger.info("createUser in user service ");
		return userRepository.save(user);
	}

	@Override
	@Transactional
	public User readUserById(int arg) {
		logger.info("readUserById in user service " + arg);
		User user = null;
		Optional<User> optional = userRepository.findById(arg);
		if (optional.isPresent()) {
			user = optional.get();
		}
		return user;
	}

	@Override
	@Transactional
	public List<User> readAllUser() {
		logger.info("readUserById in user service ");
		return userRepository.findAll();
	}
}
