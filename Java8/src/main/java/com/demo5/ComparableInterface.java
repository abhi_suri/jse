package com.demo5;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class ComparableInterface {

	public static void main(String[] args) {
		Comparable<Employee> comparable = (Employee what) -> {
			System.out.println("Inside What ");

			return (int) what.getSalary() + 5000;

		};
		int ans = comparable.compareTo(new Employee(10, "Ten", 1010.10f));
		System.out.println(ans);
		System.out.println("The End");
	}

}
