package com.main;

import java.util.Calendar;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class CalendarDemo {

	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		System.out.println(calendar.getWeekYear());
		System.out.println(calendar.getTime());
		System.out.println(calendar.get(calendar.MINUTE));

	}

}
