package com.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.model.Student;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class Demo1 {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new AnnotationConfigApplicationContext(Demo.class);
		Student student = applicationContext.getBean(Student.class);
		System.out.println(student.getStudNo());
		System.out.println(student.getStudName());
		System.out.println(student.getMarks());

	}

}
