package com.main;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com")

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class Demo {

}
