package com.exception;

import java.util.Set;

import com.model.Employee;
import com.service.EmployeeService;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class EmployeeServiceImpl implements EmployeeService {

	public EmployeeServiceImpl() {
		super();
	}

	@Override
	public Employee searchEmployeeByName(Set<Employee> set, String eName) throws EmployeeException {

		Employee dummyEmployee = null;
		for (Employee employee : set) {
			if (employee.getEmpName().equals(eName)) {
				dummyEmployee = employee;
			} else {
				throw new EmployeeException("No such Employee " + eName);
			}

		}
		return dummyEmployee;
	}

	@Override
	public Employee searchEmployeeById(Set<Employee> set, int id) throws EmployeeException {

		Employee dummyEmployee = null;
		for (Employee employee : set) {
			if (employee.getEmpId() == id) {
				dummyEmployee = employee;

			}
		}
		return dummyEmployee;
	}

}
