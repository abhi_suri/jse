package com.demo8;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class InstanceMethod {

	public void hello() {
		System.out.println("I am from hello() - method reference ");
	}

	public static void main(String[] args) {
		MyFunctionalInterface myFunctionalInterface = () -> {
			System.out.println("With lambda expression");
		};
		myFunctionalInterface.display();
		InstanceMethod instanceMethod = new InstanceMethod();
		MyFunctionalInterface interface1 = instanceMethod::hello;
		interface1.display();
		System.out.println("End");
	}

}
