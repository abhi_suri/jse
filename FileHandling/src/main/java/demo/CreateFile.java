package demo;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class CreateFile {

	public static void main(String[] args) {
		File file = new File("C:\\folder\\newfile.txt");
		boolean fvar;
		try {
			fvar = file.createNewFile();
			if (fvar) {
				System.out.println("File has been created successfully ");
			} else {
				System.out.println("File already present at the specified location ");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
