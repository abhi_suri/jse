package com.demo5;

import java.util.function.Function;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class FunctionDemo {

	public static void main(String[] args) {
		Function<Employee, String> function = (a) -> {
			return "Welcome" + a.getEmpName();
		};
		String str = function.apply(new Employee(22, " Twenty Two ", 2222.22f));
		System.out.println(str);
		System.out.println("The End");

	}

}
