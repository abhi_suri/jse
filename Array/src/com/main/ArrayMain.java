package com.main;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class ArrayMain {

	public static void main(String[] args) {
		int[] intArray = { 1, 2, 3, 4 };
		System.out.println(intArray.length);
		System.out.println(intArray[1]);

		for (int i = 0; i < intArray.length; i++) {
			System.out.println(intArray[i]);

		}

	}

}
