package com.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Employee;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class SpringMain2 {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/config/springconfig.xml");

		Employee employee = (Employee) applicationContext.getBean("employeeId");
		System.out.println(employee.getEmpNo());
		System.out.println(employee.getEmpName());
		System.out.println(employee.getSalary());

		System.out.println(employee.getAddress().getDoorNo());
		System.out.println(employee.getAddress().getCity());
		System.out.println(employee.getAddress().getState());

		((ConfigurableApplicationContext) applicationContext).registerShutdownHook();

		System.out.println("The End");

	}

}
