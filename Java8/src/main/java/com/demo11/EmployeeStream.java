package com.demo11;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class EmployeeStream {

	public static void main(String[] args) {

		Employee employee1 = new Employee(10, "Ten", 1010.10f, "A");
		Employee employee2 = new Employee(20, "Twenty", 2020.20f, "B");
		Employee employee3 = new Employee(30, "Thirty", 3030.30f, "A");
		Employee employee4 = new Employee(40, "Forty", 4040.40f, "C");

		List<Employee> arrayLists = new ArrayList<>();
		arrayLists.add(employee1);
		arrayLists.add(employee2);
		arrayLists.add(employee3);
		arrayLists.add(employee4);

		long data = arrayLists.stream().count();
		System.out.println("Count the number of records : " + data);

		/*
		 * arrayLists.stream().forEach((abc) -> {
		 * 
		 * System.out.println(abc.getEmpName());
		 * 
		 * });
		 */

		long result = arrayLists.stream().filter((arg) -> arg.getBand().equals("A"))
				.map((arg2) -> arg2.getSalary() + 5000).count();

		System.out.println("Employee in band A and have a hike of 5000: " + result);

		System.out.println("For each");
		arrayLists.stream().forEach((Employee employee) -> {

			System.out.println(employee.getSalary());

		});

	}

}
