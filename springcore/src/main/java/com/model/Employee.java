package com.model;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class Employee {
	private int empNo;
	private String empName;
	private float salary;

	private Address address;

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void Hello() {
		System.out.println("Init method() ");
	}

	public void Bye() {
		System.out.println("Destroy method() ");
	}

	public Employee() {
		super();
		System.out.println("Employee constructor ");

	}

	public Employee(int empNo, String empName, float salary) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.salary = salary;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

}
