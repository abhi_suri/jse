package com.springbootvalidation.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

@RestController
public class HelloController {

	@GetMapping(value = "add/{alias}")
	public int addTwoNumber(@PathVariable("alias") int num1) {
		int data = 0;
		if (num1 > 0) {
			data = num1 + 10;

		} else {
			throw new ArrayIndexOutOfBoundsException();

		}
		return data;
	}

}
