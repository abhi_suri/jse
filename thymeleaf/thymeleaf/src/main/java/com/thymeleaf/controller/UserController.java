package com.thymeleaf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.thymeleaf.model.User;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

@Controller
public class UserController {

	@GetMapping(value = "/")
	public String first() {
		return "index";
	}

	@PostMapping(value = "/save")
	public ModelAndView second(@ModelAttribute User user) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("success");
		modelAndView.addObject("userData", user);
		return modelAndView;
	}

}
