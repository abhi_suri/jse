package com.main;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class StringDemo1 {

	public static void main(String[] args) {
		String str1 = new String("Hello");
		String str2 = new String("Hello");
		if (str1.equals(str2)) {
			System.out.println("They are same");

		} else {
			System.out.println("They are different");

		}

	}

}
