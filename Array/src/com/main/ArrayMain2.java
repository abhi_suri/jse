package com.main;

import com.model.Employee;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class ArrayMain2 {
	public static void main(String[] args) {
		Employee employee1 = new Employee(10, "Ten", 1010.10f);
		Employee employee2 = new Employee(20, "Twenty", 2020.20f);
		Employee employee3 = new Employee(30, "Thirty", 3030.30f);

		int[] intArray = new int[3];
		Employee[] employees = new Employee[3];
		employees[0] = employee1;
		employees[1] = employee2;
		employees[2] = employee3;

		System.out.println("iterate");
		for (int i = 0; i < employees.length; i++) {
			System.out.println("Employee Number: " + employees[i].getEmployeeNumber());
			System.out.println("Employee Name: " + employees[i].getEmployeeName());
			System.out.println("Employee Salary: " + employees[i].getSalary());

		}

	}

}
