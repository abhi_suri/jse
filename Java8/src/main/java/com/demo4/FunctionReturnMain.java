package com.demo4;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class FunctionReturnMain {

	public static void main(String[] args) {
		FunctionalReturn functionalReturn = (abc) -> {
			return "Welcome " + abc;
		};
		String capture = functionalReturn.sayHello("HCL");
		System.out.println(capture);

	}

}
