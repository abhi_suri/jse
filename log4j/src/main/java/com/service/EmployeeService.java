package com.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class EmployeeService {
	private static final Logger LOGGER = LogManager.getLogger(EmployeeService.class);

	public int add(int a, int b) {
		LOGGER.info("a = " + a + " b = " + b);
		return a + b;
	}

}
