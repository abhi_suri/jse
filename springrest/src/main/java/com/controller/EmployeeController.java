package com.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.model.Employee;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

@RestController
public class EmployeeController {

	// @RequestMapping(value="create",method=RequestMethod.POST)
	@PostMapping(value = "create")
	public ResponseEntity<Employee> addEmployee(@RequestBody Employee employee) {
		System.out.println("Employee no : " + employee.getEmpNo());
		System.out.println("Employee name : " + employee.getEmpName());
		ResponseEntity<Employee> responseEntity = new ResponseEntity<Employee>(employee, HttpStatus.CREATED);
		return responseEntity;

	}

	// @RequestMapping(value="readbyid/{alias}", method=RequestMethod.GET)
	@GetMapping(value = "readbyid/{alias}")
	public ResponseEntity<Employee> getEmployeeByID(@PathVariable("alias") int empNo) {
		ResponseEntity<Employee> responseEntity = null;
		Employee employee = null;
		if (empNo == 10) {
			employee = new Employee(10, "Ten", "10 Designation");
		}
		if (empNo == 20) {
			employee = new Employee(20, "Twenty", "20 Designation");
		}
		return new ResponseEntity<Employee>(employee, HttpStatus.FOUND);
	}

	@PutMapping(value = "update/{changeName}")
	public ResponseEntity<Employee> updateEmployee(@RequestBody Employee employee, @PathVariable String changeName) {
		employee.setEmpName(changeName);
		return new ResponseEntity<Employee>(employee, HttpStatus.OK);

	}

	@DeleteMapping(value = "delete/{abc}")
	public ResponseEntity<Boolean> deleteEmployeeByID(@PathVariable("abc") int empId) {
		System.out.println("Deleted employee no : " + empId);
		return new ResponseEntity<Boolean>(true, HttpStatus.GONE);
	}

}
