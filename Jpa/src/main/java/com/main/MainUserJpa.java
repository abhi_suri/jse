package com.main;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.dao.UserDao;
import com.model.User;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class MainUserJpa {

	public static void main(String[] args) {

		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("demo1_persist_xml");
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		UserDao userDao = new UserDao(entityManager);

		User user = new User();
		// user.setId(new Integer(12));
		user.setName("World");

		Optional<User> optional = userDao.save(user);
		if (optional.isPresent()) {

			System.out.println("User id : " + optional.get().getId());
			System.out.println("User name : " + optional.get().getName());

		} else {

			System.out.println("Sorry! ");

		}

	}

}
