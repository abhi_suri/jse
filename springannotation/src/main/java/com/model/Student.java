package com.model;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Component(value = "alias")
@Service
@Repository
@PropertySource("classpath:com/config/springannotation.properties")

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class Student {

	// @Value(value="22")
	private int studNo;
	// @Value(value="Hello World")
	private String studName;
	// @Value(value="99")
	private int marks;

	@Autowired
	private Address address;

	@PostConstruct
	public void init() {
		System.out.println("Bean life cycle + init() ");
	}

	@PreDestroy
	public void destroy() {
		System.out.println("Bean life cycle + destroy() ");
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Student() {
		super();
	}

	@Autowired
	public Student(@Value("${abc}") int studNo, @Value("${def}") String studName, @Value("${ghi}") int marks) {
		super();
		this.studNo = studNo;
		this.studName = studName;
		this.marks = marks;
	}

	public int getStudNo() {
		return studNo;
	}

	public void setStudNo(int studNo) {
		this.studNo = studNo;
	}

	public String getStudName() {
		return studName;
	}

	public void setStudName(String studName) {
		this.studName = studName;
	}

	public int getMarks() {
		return marks;
	}

	public void setMarks(int marks) {
		this.marks = marks;
	}
}
