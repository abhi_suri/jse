package com.main;

import com.model.Department;

/**
*
* @author -- Abhishek Kumar
*
*/

import com.model.Employee;

public class OneToOneMapping {

	public static void main(String[] args) {
		Employee employee1 = new Employee(10, "Ten");
		Employee employee2 = new Employee(20, "Twenty");
		Employee[] employees = new Employee[2];
		employees[0] = employee1;
		employees[1] = employee2;

		Department department = new Department();
		department.setDeptId(123);
		department.setDeptName("Development");

		department.setEmployee(employees);

		System.out.println("Display");
		System.out.println("Dept No : " + department.getDeptId());
		System.out.println("Dept Name : " + department.getDeptName());
		Employee[] employeeArray = department.getEmployee();
		for (int i = 0; i < employeeArray.length; i++) {
			System.out.println("Employee No : " + employeeArray[i].getEmpNo());
			System.out.println("Employee Name : " + employeeArray[i].getEmpName());

		}

	}

}
