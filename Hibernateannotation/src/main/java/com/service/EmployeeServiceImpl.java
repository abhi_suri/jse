package com.service;

import java.util.List;

import com.bean.Employee;
import com.dao.EmployeeDao;
import com.dao.EmployeeDaoImpl;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class EmployeeServiceImpl implements EmployeeService {

	@Override
	public Employee createEmployee(Employee employee) {
		EmployeeDao employeeDao = new EmployeeDaoImpl();
		return employeeDao.createEmployee(employee);
	}

	@Override
	public List<Employee> readAllEmployees() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employee readEmployeeById(int empId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employee readEmployeeByName(String Name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employee upadteEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int deleteEmployeeById(int empId) {
		// TODO Auto-generated method stub
		return 0;
	}

}
