package demo;

import java.io.File;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class RenameFile {

	public static void main(String[] args) {

		File oldfile = new File("C:\\folder\\newfile.txt");

		File newfile = new File("C:\\folder\\renamednewfile.txt");
		boolean flag = oldfile.renameTo(newfile);
		if (flag == true) {
			System.out.println("File renamed successfully ");
		} else {
			System.out.println("Rename operation failed ");
		}
	}
}
