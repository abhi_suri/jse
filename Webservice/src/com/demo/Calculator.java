package com.demo;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class Calculator {

	public int add(int num1, int num2) {
		return num1 + num2;
	}

	public String sayHello(String name) {
		return "Welcome : " + name;
	}

}
