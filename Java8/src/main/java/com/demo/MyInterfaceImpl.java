package com.demo;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class MyInterfaceImpl implements MyInterface, AnotherInterface {

	@Override
	public int add(int a, int b) {

		return a + b;
	}

	@Override
	public int sub(int a, int b) {

		return 22;
	}

}
