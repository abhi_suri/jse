package com.demo3;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

@FunctionalInterface
public interface MyFunctional {
	public abstract void display(int a);

}
