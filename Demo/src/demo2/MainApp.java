package demo2;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class MainApp {

	public static void main(String[] args) {
		Employee abhishek = new Employee();
		abhishek.empNo = 51978481;
		abhishek.empName = "Abhishek";
		abhishek.experience = 2f;
		abhishek.band = "E1";
		abhishek.location = "Bangalore";
		abhishek.salary = 20000f;
		abhishek.display();
		System.out.println("Employee No : " + abhishek.empNo);
		System.out.println("Employee Name : " + abhishek.empName);
		System.out.println("Employee Experience : " + abhishek.experience);
		System.out.println("Employee Band : " + abhishek.band);
		System.out.println("Employee Location : " + abhishek.location);
		System.out.println("Employee Salary : " + abhishek.salary);

	}

}
