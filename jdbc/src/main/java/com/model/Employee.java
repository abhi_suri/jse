package com.model;

import java.io.Serializable;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class Employee implements Serializable {

	private int employeeNo;
	private String empName;
	private float salary;
	private int age;

	public Employee() {
		super();
	}

	public Employee(int employeeNo, String empName, float salary, int age) {
		super();
		this.employeeNo = employeeNo;
		this.empName = empName;
		this.salary = salary;
		this.age = age;
	}

	public int getEmployeeNo() {
		return employeeNo;
	}

	public void setEmployeeNo(int employeeNo) {
		this.employeeNo = employeeNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

}
