package com.model;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class Jio implements Recharge, DataRecharge {

	@Override
	public int Rs50() {
		System.out.println("65 SMS free - Jio ");
		System.out.println("Tax 12.5% deduct - Jio ");
		return 35;
	}

	@Override
	public int Rs100() {
		System.out.println("120 SMS free - Jio ");
		System.out.println("Tax 17.5% deduct - Jio ");
		return 88;
	}

	@Override
	public String dataFor50() {
		return "Internet for 6 days - Jio ";
	}

	@Override
	public String dataFor100() {
		return "Internet for 12 days - Jio ";
	}

}
