package com.demo8;

/**
 *
 * @author -- Abhishek Kumar
 *
 */
@FunctionalInterface
public interface MyFunctionalInterface {

	public abstract void display();

}
