package com.demo;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class MainDefault {

	public static void main(String[] args) {
		MyInterface myInterface = new MyInterfaceImpl();
		int ans = myInterface.add(4, 6);
		System.out.println("Addition of two numbers : " + ans);
		System.out.println("Static final variable : " + myInterface.PI);
		System.out.println("Subtraction of two numbers : " + myInterface.sub(3, 2));
		System.out.println("Multiplication of two numbers : " + MyInterface.mul(2, 3));
	}

}
