package com.main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.service.EmployeeService;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class MainApplication {

	private static final Logger LOGGER = LogManager.getLogger(MainApplication.class);

	public static void main(String[] args) {

		LOGGER.info("You are in main () ");
		LOGGER.warn("warning!");
		LOGGER.error("error");
		EmployeeService employeeService = new EmployeeService();
		int ans = employeeService.add(5, 6);
		System.out.println("Addition of two numbers: " + ans);
		LOGGER.info("Data received from service : " + ans);
		System.out.println("The End");

	}

}
