package com;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class ThreadDemoMain {

	public static void main(String[] args) {
		ThreadDemo threadDemo = new ThreadDemo();
		threadDemo.start();
		System.out.println(threadDemo.getPriority());
		threadDemo.setPriority(6);
		System.out.println("After: " + threadDemo.getPriority());
		try {
			threadDemo.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
