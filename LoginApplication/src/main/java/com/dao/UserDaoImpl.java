package com.dao;

import com.model.User;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class UserDaoImpl implements UserDao {

	@Override
	public int deleteUserByUserId(int empId) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public User createUser(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User readUserById(int userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User updateUser(User user) {
		return null;
	}

	@Override
	public User validateUserIdAndPassword(int userId, String password) {
		User user = null;
		if (userId == 123456 && password.equals("Hello123#")) {
			user = new User(userId, "World", password);
		}
		if (userId == 112233 && password.equals("Secret123#")) {
			user = new User(userId, "Happy", password);
		}
		return user;
	}
}
