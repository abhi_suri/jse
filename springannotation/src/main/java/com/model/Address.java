package com.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

@Component
public class Address {
	@Value(value = "123")
	private int doorNo;
	@Value(value = "Bangalore")
	private String city;
	@Value(value = "Karnataka")
	private String state;

	public Address() {
		super();
		System.out.println("Address constructor ");
	}

	public Address(int doorNo, String city, String state) {
		super();
		this.doorNo = doorNo;
		this.city = city;
		this.state = state;
	}

	public int getDoorNo() {
		return doorNo;
	}

	public void setDoorNo(int doorNo) {
		this.doorNo = doorNo;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}
