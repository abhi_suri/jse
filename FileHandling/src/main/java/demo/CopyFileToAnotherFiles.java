package demo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class CopyFileToAnotherFiles {

	public static void main(String[] args) {

		FileInputStream instream = null;
		FileOutputStream outputstream = null;

		try {
			File infile = new File("C:\\folder\\renamednewfile.txt");
			File outfile = new File("C:\\folder\\copyOutputFile.txt");

			instream = new FileInputStream(infile);
			outputstream = new FileOutputStream(outfile);

			byte[] buffer = new byte[1024];
			int length;

			while ((length = instream.read(buffer)) > 0) {
				outputstream.write(buffer, 0, length);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
