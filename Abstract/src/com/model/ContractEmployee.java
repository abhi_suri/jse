package com.model;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class ContractEmployee extends Employee {

	@Override
	public float calculateSalary() {

		return 1200f;
	}

	public String getName(String arg) {
		return "Welcome : " + arg;
	}

}
