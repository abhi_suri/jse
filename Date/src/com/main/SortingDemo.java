package com.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import com.model.Employee;
import com.model.SalaryCompare;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class SortingDemo {
	public static void main(String[] args) {
		Employee employee1 = new Employee(22, "Twenty Two", 1000f);
		Employee employee2 = new Employee(100, "Hundred", 5000f);
		Employee employee3 = new Employee(5, "Five", 3000f);
		Employee employee4 = new Employee(30, "Thirty", 5000f);

		List<Employee> listOfEmployee = new ArrayList<Employee>();
		listOfEmployee.add(employee1);
		listOfEmployee.add(employee2);
		listOfEmployee.add(employee3);
		listOfEmployee.add(employee4);

		Collections.sort(listOfEmployee, new SalaryCompare());

		for (Employee employee : listOfEmployee) {
			System.out.print(employee.getEmpNo() + " ");
			System.out.print(employee.getEmpName() + " ");
			System.out.println(employee.getSalary());

		}

	}

}
