package com.main;

import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import com.dao.UserDao;
import com.model.User;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class MainUserJpaRead1 {

	public static void main(String[] args) {

		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("demo1_persist_xml");
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		UserDao userDao = new UserDao(entityManager);

		Optional<User> optional = userDao.findById(2);
		User user = optional.get();

		System.out.println("User id : " + user.getId());
		System.out.println("User name : " + user.getName());

	}

}
