package com.main;

import com.model.Airtel;
import com.model.DataRecharge;
import com.model.Recharge;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class Customer {
	public static void main(String[] args) {
		Recharge recharge = new Airtel();
		int talkTime = recharge.Rs50();

		DataRecharge dataRecharge = new Airtel();

		System.out.println("For Internet : " + dataRecharge.dataFor50());
		System.out.println("Over all talktime : " + talkTime);
	}

}
