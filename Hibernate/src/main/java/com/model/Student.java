package com.model;

import java.io.Serializable;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class Student implements Serializable {

	private int studeNo;
	private String studeName;
	private int marks;

	public Student() {
		super();
	}

	public Student(int studeNo, String studeName, int marks) {
		super();
		this.studeNo = studeNo;
		this.studeName = studeName;
		this.marks = marks;
	}

	public int getStudeNo() {
		return studeNo;
	}

	public void setStudeNo(int studeNo) {
		this.studeNo = studeNo;
	}

	public String getStudeName() {
		return studeName;
	}

	public void setStudeName(String studeName) {
		this.studeName = studeName;
	}

	public int getMarks() {
		return marks;
	}

	public void setMarks(int marks) {
		this.marks = marks;
	}

}
