package demo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class WriteToFileOutputStreamUpdate {

	public static void main(String[] args) {

		FileOutputStream fos = null;
		File file;
		String mycontent = "This is my data which needs " + " to be written into the file ";
		file = new File("C:\\folder\\renamednewfile.txt");
		try {
			fos = new FileOutputStream(file, true);
			if (!file.exists()) {
				try {
					file.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				byte[] bytesArray = mycontent.getBytes();
				fos.write(bytesArray);
				fos.flush();
				System.out.println("File written successfully ");
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fos != null) {
					fos.close();
				}

			} catch (IOException ioe) {
				System.out.println("Error in closing the stream ");
			}
		}
	}
}
