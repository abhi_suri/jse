package com.main;

import com.model.Calculator;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class CalculatorMain {

	public static void main(String[] args) {
		Calculator calculator = new Calculator();
		System.out.println("Adding : " + calculator.add(2, 3));

	}

}
