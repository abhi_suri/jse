package com.h2.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.ConstraintViolationException;
import javax.validation.constraints.Size;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.h2.model.User;
import com.h2.service.UserService;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

@RestController
@RequestMapping("user")
@Slf4j
@Validated
public class UserController {

	@Autowired
	private UserService userService;
	private static Logger logger = LoggerFactory.getLogger(UserController.class);

	@PostMapping(value = "/create")
	public ResponseEntity<User> storeUser(@RequestBody User user) {
		logger.info("storeUser in controller ");
		User user2 = userService.createUser(user);
		return new ResponseEntity<User>(user2, HttpStatus.CREATED);
	}

	@GetMapping(value = "read/{userId}")
	public ResponseEntity<User> readByUserId(@PathVariable int userId) {
		logger.info("readByUserId in controller " + userId);
		User user = userService.readUserById(userId);
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	@GetMapping(value = "readall")
	public ResponseEntity<List<User>> readAll() {
		logger.info("readAll in controller ");
		return new ResponseEntity<List<User>>(userService.readAllUser(), HttpStatus.OK);
	}

	@GetMapping(value = "hello/{name}")
	public ResponseEntity<String> sayHello(
			@PathVariable @Size(min = 3, message = "Name should have min 3 characters in path") String name) {
		logger.info("sayHello() in controller ");
		return new ResponseEntity<String>(name + "Welcome", HttpStatus.OK);
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(ConstraintViolationException.class)
	public Map<String, String> handleConstraintViolation(ConstraintViolationException ex) {
		Map<String, String> errors = new HashMap<>();

		ex.getConstraintViolations().forEach(cv -> {
			errors.put("message", cv.getMessage());
			errors.put("path", (cv.getPropertyPath()).toString());
		});

		return errors;
	}
}
