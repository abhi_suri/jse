package com.main;

import com.model.ContractEmployee;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class MainEmployee {

	public static void main(String[] args) {

		ContractEmployee contractEmployee = new ContractEmployee();

		System.out.println(contractEmployee.calculateSalary());
		System.out.println(contractEmployee.getName("HCL"));

	}

}
