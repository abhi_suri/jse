package com.demo1;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class Lambda {

	public static void main(String[] args) {
		MyFunctional myFunctional = () -> System.out.println("Welcome to Lambda Expression ");
		// myFunctional.display();
		Lambda.sayHello();
		Lambda.sayWorld(myFunctional);

	}

	public static void sayHello() {
		System.out.println("Say hello() ");
	}

	public static void sayWorld(MyFunctional var) {
		var.display();
		System.out.println("Say world() ");
	}

}
