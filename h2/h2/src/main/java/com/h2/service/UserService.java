package com.h2.service;

/**
*
* @author -- Abhishek Kumar
*
*/

import java.util.List;

import com.h2.model.User;

public interface UserService {

	public abstract User createUser(User user);

	public abstract User readUserById(int arg);

	public abstract List<User> readAllUser();

}
