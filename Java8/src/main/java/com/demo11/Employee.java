package com.demo11;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class Employee {

	private int empNo;
	private String empName;
	private float salary;
	private String band;

	public Employee() {
		super();
	}

	public Employee(int empNo, String empName, float salary, String band) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.salary = salary;
		this.band = band;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	public String getBand() {
		return band;
	}

	public void setBand(String band) {
		this.band = band;
	}

}
