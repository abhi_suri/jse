package com.main;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class StringBufferDemo {

	public static void main(String args[]) {
		String var1 = "Hello";
		System.out.println("String length : " + var1.length());

		StringBuffer var2 = new StringBuffer();
		var2.append("Hello");
		System.out.println("StringBuffer length : " + var2.length());
		System.out.println("StringBuffer capacity : " + var2.capacity());
		var2.append("World");
		System.out.println("After append : " + var2);
		System.out.println("After append StringBuffer length : " + var2.length());
		System.out.println("After append StringBuffer capacity : " + var2.capacity());
		var2.append("1234567");
		System.out.println("After append1234567 : " + var2);
		System.out.println("After append1234567 StringBuffer length : " + var2.length());
		System.out.println("After append1234567 StringBuffer capacity : " + var2.capacity());
		System.out.println(var2.reverse());

		StringBuilder var3 = new StringBuilder("Hello");

		System.out.println("StringBuilder length : " + var3);
		System.out.println("After append StringBuilder length : " + var3.length());
		System.out.println("After append StringBuilder capacity : " + var3.capacity());

	}

}
