package com.hcl.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

@Controller
public class MyController {

	@GetMapping(value = "/hai")
	public String sayHello() {
		return "success";
	}

}
