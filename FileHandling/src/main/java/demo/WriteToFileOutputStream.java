package demo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class WriteToFileOutputStream {

	public static void main(String[] args) {

		FileOutputStream fos = null;
		File file;
		String mycontent = "This is my Data which needs " + " to be written into the file ";
		file = new File("C:\\folder\\renamednewfile.txt");
		try {
			fos = new FileOutputStream(file);
			if (!file.exists()) {
				try {
					file.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}

			} else {
				byte[] bytesArray = mycontent.getBytes();
				fos.write(bytesArray);
				fos.flush();
				System.out.println("File written successfully");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
