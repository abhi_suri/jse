package com.springboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.model.Address;
import com.springboot.service.AddressService;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

@RestController
public class AddressController {
	@Autowired
	private AddressService addressService;

	@GetMapping(value = "/")
	public String sayHello() {
		return "Welcome to Spring Boot ";
	}

	@PostMapping(value = "create")
	public ResponseEntity<Address> createAddress(@RequestBody Address address) {
		Address dummyAddress = null;
		if (address != null) {
			dummyAddress = addressService.addAddress(address);
		}
		return new ResponseEntity<Address>(dummyAddress, HttpStatus.CREATED);
	}

	@GetMapping(value = "readbyid/{doorNo}")
	public Address readAddressByDoorNo(@PathVariable int doorNo) {
		Address address = null;
		if (doorNo > 0) {
			address = addressService.readAddressByDoorNo(doorNo);
		}
		return address;
	}

	@PutMapping(value = "/update")
	public Address updateAddress(@RequestBody Address address) {
		return addressService.alterAddress(address);
	}

	@DeleteMapping(value = "/delete/{doorNo}")
	public int deleteAddress(@PathVariable("doorNo") int doorNo) {
		return addressService.removeAddress(doorNo);
	}

	@GetMapping(value = "custom/{alias}")
	public ResponseEntity<String> customQuery(@PathVariable("alias") int doorNo) {
		String str = addressService.getStateById(doorNo);
		return new ResponseEntity<String>(str, HttpStatus.OK);
	}

	@GetMapping(value = "native")
	public ResponseEntity<List<Address>> nativeQuery() {

		return new ResponseEntity<List<Address>>(addressService.nativeQuery(), HttpStatus.OK);
	}

}
