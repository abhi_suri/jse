package com.demo4;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public interface FunctionalReturn {

	public String sayHello(String name);

}
