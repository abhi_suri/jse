package com.service;

import java.util.Set;

import com.exception.EmployeeException;
import com.model.Employee;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public interface EmployeeService {

	Employee searchEmployeeByName(Set<Employee> set, String eName) throws EmployeeException;

	Employee searchEmployeeById(Set<Employee> set, int id) throws EmployeeException;

}