package com.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Student;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class Demo2 {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/config/springconfig.xml");
		Student student = (Student) applicationContext.getBean("alias");
		System.out.println("Spring with XML and Annotation + @Component + Value + constructor injection ");
		System.out.println(student.getStudNo());
		System.out.println(student.getStudName());
		System.out.println(student.getMarks());
		System.out.println(student.getAddress().getState());
	}
}
