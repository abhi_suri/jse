package com.main;

import com.model.ContractEmployee;
import com.model.Employee;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class MainApp {
	public static void main(String[] args) {
		Employee employee = new Employee();
		employee.getEmpNo();
		employee.getEmpNo();

		ContractEmployee contractEmployee = new ContractEmployee();
		contractEmployee.getEmpNo();
		contractEmployee.getEmpName();
		contractEmployee.sayHello();

	}

}
