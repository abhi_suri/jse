package demo;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class ReadFromFile {

	public static void main(String[] args) throws IOException {

		InputStream is = new FileInputStream("C:\\folder\\renamednewfile.txt");
		int c;
		while ((c = is.read()) != -1) {
			System.out.print((char) c);
		}
	}
}
