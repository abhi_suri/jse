package com.service;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class Demo {

	public String sayHello() {
		return "Welcome";
	}

	public String getName(String arg) {
		String dummy = null;
		if (arg.length() > 5) {
			dummy = "Welcome" + arg;
		} else {
			throw new ArrayIndexOutOfBoundsException();
		}
		return dummy;
	}

}
