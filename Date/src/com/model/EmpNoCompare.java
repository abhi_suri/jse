package com.model;

import java.util.Comparator;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class EmpNoCompare implements Comparator<Employee> {

	@Override
	public int compare(Employee o1, Employee o2) {
		if (o1.getEmpNo() < o2.getEmpNo())
			return -1;
		if (o1.getEmpNo() > o2.getEmpNo())
			return 1;
		else
			return 0;
	}

}
