package com.main;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import com.model.Student;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class Main3 {

	public static void main(String[] args) {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure().build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();

		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();

		// String hqlRead = "select stud.studeName from Student as stud";//

		String hqlRead = "from Student where studeNo > 0 ORDER BY studeNo DESC";
		org.hibernate.query.Query<Student> query = session.createQuery(hqlRead);
		// query.setInteger("abc", 21);
		List<Student> studentList = query.list();
		for (Student student : studentList) {
			System.out.println(student.getStudeNo());
			System.out.println(student.getStudeName());
			System.out.println(student.getMarks());

		}

		System.out.println("End of Hibernate ");

	}

}
