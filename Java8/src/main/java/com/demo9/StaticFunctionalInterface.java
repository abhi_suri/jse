package com.demo9;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public interface StaticFunctionalInterface {

	public int add(int a, int b);

}
