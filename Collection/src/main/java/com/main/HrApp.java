package com.main;

import java.util.HashSet;
import java.util.Set;

import com.exception.EmployeeException;
import com.exception.EmployeeServiceImpl;
import com.model.Employee;
import com.service.EmployeeService;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class HrApp {

	public static void main(String[] args) {

		Employee employee1 = new Employee(10, "Ten", 1010.10f);
		Employee employee2 = new Employee(20, "Twenty", 2020.20f);
		Employee employee3 = new Employee(30, "Thirty", 3030.30f);
		Employee employee4 = new Employee(40, "Forty", 4040.40f);

		Set<Employee> employees = new HashSet();
		employees.add(employee1);
		employees.add(employee2);
		employees.add(employee3);
		employees.add(employee4);

		EmployeeService employeeService = new EmployeeServiceImpl();
		try {

			Employee employee = employeeService.searchEmployeeById(employees, 10);
			if (employee != null) {
				System.out.println(employee.getEmpId());
				System.out.println(employee.getEmpName());
				System.out.println(employee.getSalary());
			} else {
				System.out.println("No Result to show ");
			}

		} catch (EmployeeException e) {
			System.err.println(e.getMessage());
		}
	}

}
