package com.main;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class DateDemo {

	public static void main(String[] args) {
		Date todayDate = new Date();
		System.out.println(todayDate);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		System.out.println(dateFormat.format(todayDate));

	}

}
