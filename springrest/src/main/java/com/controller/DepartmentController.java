package com.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.model.Department;
import com.model.Employee;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

@RestController
public class DepartmentController {

	@RequestMapping(value = "one", method = RequestMethod.GET)
	public String sayHello() {
		return "Welcome to Spring Rest ";
	}

	@RequestMapping(value = "two", method = RequestMethod.GET, produces = {
			org.springframework.http.MediaType.APPLICATION_JSON_VALUE,
			org.springframework.http.MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Department> getDepartment() {
		Department department = new Department(10, "Development", "Karnataka");
		Employee employee1 = new Employee(123, "One Two Three", "Developer");
		Employee employee2 = new Employee(45, "Four Five", "Tester");
		Employee employee3 = new Employee(67, "Six Seven", "Designer");
		List<Employee> employeeList = new ArrayList<Employee>();
		employeeList.add(employee1);
		employeeList.add(employee2);
		employeeList.add(employee3);
		department.setEmployees(employeeList);

		ResponseEntity<Department> responseEntity = new ResponseEntity<Department>(department, HttpStatus.CREATED);
		return responseEntity;
	}

}
