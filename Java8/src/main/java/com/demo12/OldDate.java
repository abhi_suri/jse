package com.demo12;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class OldDate {

	public static void main(String[] args) {
		System.out.println(new Date());
		Date date = new Date();

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		System.out.println(simpleDateFormat.format(date));

	}

}
