package dao;

import java.util.List;

import com.model.Employee;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public interface EmployeeDao {

	public abstract Employee createEmployee(Employee employee);

	public abstract List<Employee> readAllEmployee();

	public abstract Employee readEmployeeById(int id);

	public abstract Employee readEmployeeByName(String name);

	public abstract Employee updateEmployee(Employee employee);

	public abstract int deleteEmployeeById(int id);

	public abstract Employee deleteEmployeeName(String name);

}
