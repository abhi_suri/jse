package com.main;

import com.model.Employee;
import com.service.EmployeeService;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class MainApp1 {

	public static void main(String[] args) {
		Employee employee = new Employee(12, "World", 34.56f);

		Employee employee1 = new Employee(34, "Happy", 2000f);

		EmployeeService employeeService = new EmployeeService();
		Employee obj = employeeService.updateSalary(employee1, 1000);
		System.out.println(obj.getEmployeeNumber());
		System.out.println(obj.getEmployeeName());
		System.out.println(obj.getSalary());

		employee = null;

	}

}
