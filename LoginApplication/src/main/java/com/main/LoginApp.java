package com.main;

import java.util.Scanner;

import com.exception.CustomException;
import com.model.User;
import com.service.UserService;
import com.service.UserServiceImpl;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class LoginApp {

	public static void main(String[] args) {
		System.out.println("Login Application");

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter User Id : ");
		int id = scanner.nextInt();

		System.out.println("Enter Password : ");
		String pwd = scanner.next();

		UserService userService = new UserServiceImpl();
		User user;
		try {
			user = userService.validateUserIdAndPassword(id, pwd);
			if (user != null) {
				System.out.println("User Id : " + user.getUserId());
				System.out.println("User Name : " + user.getUserName());
				System.out.println("Password : " + user.getPassword());

			}
		} catch (CustomException e) {
			System.err.println(e.getMessage());
		}

	}

}
