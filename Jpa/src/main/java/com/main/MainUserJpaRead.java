package com.main;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import com.dao.UserDao;
import com.model.User;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class MainUserJpaRead {

	public static void main(String[] args) {

		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("demo1_persist_xml");
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		UserDao userDao = new UserDao(entityManager);

		List<User> users = userDao.findAll();
		for (User user : users) {

			System.out.println("User id : " + user.getId());
			System.out.println("User name : " + user.getName());

		}

	}

}
