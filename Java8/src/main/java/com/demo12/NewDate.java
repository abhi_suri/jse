package com.demo12;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class NewDate {

	public static void main(String[] args) {
		System.out.println(LocalDate.now());
		System.out.println(LocalDateTime.now());
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		System.out.println(LocalDate.now().getYear());
		System.out.println(LocalDate.now().format(dateTimeFormatter));
		LocalDate date1 = LocalDate.of(2020, 10, 18);
		LocalDate date2 = LocalDate.of(2021, 10, 18);
		System.out.println(ChronoUnit.DAYS.between(date1, date2));

	}

}
