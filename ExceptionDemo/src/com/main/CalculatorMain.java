package com.main;

import com.service.Calculator;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class CalculatorMain {

	public static void main(String[] args) {
		Calculator calculator = new Calculator();
		try {
			int var1 = Integer.parseInt(args[0]);
			int var2 = Integer.parseInt(args[1]);

			int ans = calculator.div(var1, var2);
			System.out.println("Answer : " + ans);
		}

		catch (java.lang.ArithmeticException e) {
			System.err.println("Solution " + e.getMessage());
		}

		catch (java.lang.ArrayIndexOutOfBoundsException aiobe) {

			System.err.println("Please enter the argument " + aiobe.getMessage());
		}

		catch (java.lang.NumberFormatException var) {

			System.err.println("Please enter the argument " + var.getMessage());
		}

		catch (java.lang.Exception bg) {

			System.err.println("Global Exception " + bg.getMessage());
		}

		System.out.println("The End");

	}

}
