package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import com.model.Employee;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class EmployeeDaoImpl implements EmployeeDao {

	@Override
	public Employee createEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Employee> readAllEmployee() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employee readEmployeeById(int id) {

		Employee employee = null;

		ResultSet resultSet;
		String query = "SELECT * FROM employee WHERE Emp_no = ?";
		Connection connection = TestMysql.getConnection();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, id);
			resultSet = preparedStatement.executeQuery();
			employee = new Employee();

			while (resultSet.next()) {
				/*
				 * System.out.println("Employee number : " + resultSet.getInt("Emp_no"));
				 * System.out.println("Employee name : " + resultSet.getString("Emp_name"));
				 * System.out.println("Employee salary  : " + resultSet.getFloat("Salary"));
				 */
				employee.setEmployeeNo(resultSet.getInt("Emp_no"));
				employee.setEmpName(resultSet.getString("Emp_name"));
				employee.setAge((int) resultSet.getFloat("Salary"));

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return employee;
	}

	@Override
	public Employee readEmployeeByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employee updateEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int deleteEmployeeById(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Employee deleteEmployeeName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

}
