package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

@Controller
public class Demo {

	@RequestMapping(value = "abc")
	public String functionOne() {
		return "Success";
	}

	@RequestMapping(value = "def")
	public String functionTwo() {
		return "Thankyou";
	}

}
