package com.demo5;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class RunnableFunctional {

	public static void main(String[] args) {
		Runnable runnable = () -> {
			System.out.println("Runnable Interface ");
		};
		runnable.run();
		System.out.println("The End");

	}

}
