package com.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.bean.Employee;
import com.utility.HibernateUtil;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class EmployeeDaoImpl implements EmployeeDao {

	@Override
	public Employee createEmployee(Employee employee) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = session.beginTransaction();
		session.save(employee);
		transaction.commit();
		return employee;
	}

	@Override
	public List<Employee> readAllEmployees() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		String readAllEmployee = "From Employee";
		Transaction transaction = session.beginTransaction();
		List<Employee> employees = session.createQuery(readAllEmployee).list();
		transaction.commit();

		return employees;

	}

	@Override
	public Employee readEmployeeById(int empId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = session.beginTransaction();
		Employee employee = session.load(Employee.class, empId);
		return employee;

	}

	@Override
	public Employee readEmployeeByName(String Name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employee upadteEmployee(Employee employee) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = session.beginTransaction();
		session.update(employee);
		transaction.commit();
		return employee;
	}

	@Override
	public int deleteEmployeeById(int empId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		// if( the record exist)
		Transaction transaction = session.beginTransaction();
		session.delete(session.get(Employee.class, empId));
		transaction.commit();
		return 1;
	}

}
