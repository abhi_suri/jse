package com.demo9;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class Main {

	public static void main(String[] args) {

		StaticFunctionalInterface staticFunctionalInterface = (aa, bb) -> {
			return aa + bb;

		};

		System.out.println(staticFunctionalInterface.add(3, 4));

		StaticFunctionalInterface staticFuntionalIinterface2 = StaticClass::addingTwoNumbers;
		int ans = staticFuntionalIinterface2.add(20, 25);
		System.out.println("With method reference : " + ans);
		System.out.println("The End");

	}

}
