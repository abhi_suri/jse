package com.service;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class CalculatorTest {

	static Calculator calculator = null;

	@BeforeClass
	public static void startTest() {
		calculator = new Calculator();
		System.out.println("It is executed only once -- before ");

	}

	@Test
	public void testAdd() {
		assertEquals(5, calculator.add(2, 3));
	}

	@Test
	public void testAdd1() {

		assertEquals(8, calculator.add(5, 3));
	}

	@AfterClass
	public static void stopTest() {

		calculator = null;
		System.out.println("It is executed only once -- after ");

	}

}
