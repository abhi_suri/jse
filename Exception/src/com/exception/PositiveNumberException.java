package com.exception;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

@SuppressWarnings("serial")
public class PositiveNumberException extends Exception {

	private String whatMessage;

	public PositiveNumberException(String whatMessage) {
		super();
		this.whatMessage = whatMessage;
	}

	@Override
	public String getMessage() {

		return this.whatMessage;
	}

}
