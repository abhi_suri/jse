package com.service;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class DemoTest {

	@Test
	public void testSayHello() {
		Demo demo = new Demo();
		assertEquals("Welcome", demo.sayHello());
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void testGetName() {
		Demo demo = new Demo();
		assertEquals("WelcomeHCL", demo.getName("HCL"));
	}

}
