package com.main;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import com.model.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class Main2 {

	public static void main(String[] args) {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure().build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();

		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();

		try {

			Student student = session.load(Student.class, 55);

			System.out.println("Student no : " + student.getStudeNo());
			System.out.println("Student name : " + student.getStudeName());
			System.out.println("Student marks : " + student.getMarks());

		} catch (org.hibernate.ObjectNotFoundException e) {
			System.err.println("No such record ");
		}

		System.out.println("End of Hibernate ");

	}

}
