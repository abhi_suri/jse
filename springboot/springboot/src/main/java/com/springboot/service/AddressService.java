package com.springboot.service;

/**
*
* @author -- Abhishek Kumar
*
*/

import java.util.List;

import com.springboot.model.Address;

public interface AddressService {

	public abstract Address addAddress(Address address);

	public abstract Address readAddressByDoorNo(int doorNo);

	public abstract Address readAddressByCity(String city);

	public abstract Address readAddressByState(String state);

	public abstract Address alterAddress(Address address);

	public abstract int removeAddress(int doorNo);

	public abstract String getStateById(int doorNo);

	public abstract List<Address> nativeQuery();

}
