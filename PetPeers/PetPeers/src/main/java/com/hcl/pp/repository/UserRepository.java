package com.hcl.pp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.hcl.pp.model.User;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public interface UserRepository extends JpaRepository<User, Integer> {

	User findByUserName(String userName);

	User findByUserNameAndUserPassword(String userName, String password);

	User findById(Long userId);

}
