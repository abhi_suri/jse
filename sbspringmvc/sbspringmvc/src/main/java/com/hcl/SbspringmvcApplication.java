package com.hcl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbspringmvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbspringmvcApplication.class, args);
	}

}
