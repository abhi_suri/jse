package com.model;

import java.util.Comparator;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class NameCompare implements Comparator<Employee> {

	@Override
	public int compare(Employee o1, Employee o2) {
		return o1.getEmpName().compareTo(o2.getEmpName());
	}

}
