package com.demo6;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class ForEach {
	public static void main(String[] args) {
		Department department = new Department(10, "Development");
		Department department1 = new Department(20, "Testing");
		Department department2 = new Department(30, "User interface - JS");

		List<Department> departments = new ArrayList();
		departments.add(department);
		departments.add(department1);
		departments.add(department2);

		departments.forEach((abc) -> {
			System.out.println("Department number : " + abc.getDeptNo());
			System.out.println("Department name : " + abc.getDeptName());
		});

	}

}
