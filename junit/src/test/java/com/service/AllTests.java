package com.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

@RunWith(Suite.class)
@SuiteClasses({ CalculatorTest.class, DemoTest.class })
public class AllTests {

}
