package com.demo3;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class Lambda {

	public static void main(String[] args) {
		MyFunctional myFunctional = (var) -> {
			System.out.println("Welcome to Lambda Expression with argument : " + var);
			System.out.println("Welcome to Lambda Expression with argument : " + var);
		};
		myFunctional.display(33);

	}

}
