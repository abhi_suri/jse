package com.demo;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public interface MyInterface {

	public static final float PI = 3.1415f;

	public abstract int add(int a, int b);

	public default int sub(int a, int b) {

		return a - b;
	}

	public static int mul(int a, int b) {
		return a * b;
	}

}
