package com.dao;
/**
*
* @author -- Abhishek Kumar
*
*/

import java.util.List;

import com.bean.Employee;

public interface EmployeeDao {
	
	public abstract Employee createEmployee(Employee employee);
	public abstract List<Employee> readAllEmployees();
	public abstract Employee readEmployeeById(int empId);
	public abstract Employee readEmployeeByName(String Name);
	public abstract Employee upadteEmployee(Employee employee);
	public abstract int deleteEmployeeById(int empId);

}
