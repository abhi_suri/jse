package com.demo9;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class StaticClass {

	public static int addingTwoNumbers(int num1, int num2) {
		return num1 + num2;
	}

}
