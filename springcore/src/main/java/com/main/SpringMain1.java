package com.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Address;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class SpringMain1 {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/config/springconfig.xml");
		Address address = (Address) applicationContext.getBean("address");
		address.setDoorNo(123);
		address.setCity("Darbhanga");
		address.setState("Bihar");

		System.out.println("Door no : " + address.getDoorNo());
		System.out.println("City : " + address.getCity());
		System.out.println("State : " + address.getState());

	}

}
