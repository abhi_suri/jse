package com.demo5;

import java.util.function.BiConsumer;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class BiConsumerDemo {

	public static void main(String[] args) {
		BiConsumer<Integer, String> biConsumer = (Integer a, String b) -> {

			System.out.println("Value of Integer : " + (a + 20));
			System.out.println("Value of String : " + b + b.length());

		};
		biConsumer.accept(100, "Hello");
		System.out.println("The End ");

	}

}
