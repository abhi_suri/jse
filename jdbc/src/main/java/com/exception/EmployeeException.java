package com.exception;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class EmployeeException extends Exception {

	@Override
	public String getMessage() {

		return this.message;
	}

	private String message;

	public EmployeeException(String message) {
		super();
		this.message = message;
	}

}
