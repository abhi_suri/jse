package com.springbootvalidation.controller;

import java.time.LocalDate;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.springbootvalidation.model.Employee;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

@RestController
public class EmployeeController {

	@PostMapping(value = "create")
	public ResponseEntity<Employee> saveEmployee(@Valid @RequestBody Employee employee) {
		System.out.println("Employee number : " + employee.getEmpNo());
		System.out.println("Employee name : " + employee.getEmpName());
		System.out.println("Employee salary : " + employee.getSalary());
		System.out.println("Employee DOB : " + employee.getDob());
		System.out.println("Employee email : " + employee.getEmail());
		return new ResponseEntity<Employee>(employee, HttpStatus.CREATED);
	}

	@GetMapping("/")
	public Employee getEmployee() {
		return new Employee(10, "Hello", 1010f, LocalDate.now(), "hello@email.com");
	}

	/*
	 * @ExceptionHandler(MethodArgumentNotValidException.class) public
	 * ResponseEntity<CustomError> handleException() { CustomError customError = new
	 * CustomError(LocalDateTime.now(), "Some message", "Please check"); return new
	 * ResponseEntity<CustomError>(customError, HttpStatus.BAD_REQUEST); }
	 */

}
