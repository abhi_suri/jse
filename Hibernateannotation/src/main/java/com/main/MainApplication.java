package com.main;

import com.dao.EmployeeDao;
import com.dao.EmployeeDaoImpl;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class MainApplication {
	public static void main(String[] args) {

		EmployeeDao employeeDao = new EmployeeDaoImpl();
		/*
		 * Employee employee = new Employee("Three", 333.33f);
		 * employeeDao.createEmployee(employee);
		 */

		/*
		 * List<Employee> listOfEmployees = employeeDao.readAllEmployees(); for
		 * (Employee employee : listOfEmployees) { System.out.println("Employee no : " +
		 * employee.getEmpNo()); System.out.println("Employee name : " +
		 * employee.getEmpName()); System.out.println("Employee salary : " +
		 * employee.getSalary());
		 * 
		 * 
		 * 
		 * }
		 */

		/*
		 * Employee employee = employeeDao.readEmployeeById(3);
		 * System.out.println("Employee no : " + employee.getEmpNo());
		 * System.out.println("Employee name : " + employee.getEmpName());
		 * System.out.println("Employee salary : " + employee.getSalary());
		 */

		/*
		 * Employee employee = employeeDao.upadteEmployee(new Employee(3,
		 * "Three updated", 333.33f)); System.out.println("Employee no : " +
		 * employee.getEmpNo()); System.out.println("Employee name : " +
		 * employee.getEmpName()); System.out.println("Employee salary : " +
		 * employee.getSalary());
		 */

		int i = employeeDao.deleteEmployeeById(3);
		if (i == 1) {
			System.out.println("Employee " + 3 + "is deleted");
		} else {
			System.out.println("No operation is done! ");
		}

	}
}
