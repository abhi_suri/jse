package com.service;

import com.model.Employee;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class EmployeeService {

	public Employee updateSalary(Employee employee, float salaryHike)

	{
		employee.setSalary(employee.getSalary() + salaryHike);
		return employee;

	}

}
