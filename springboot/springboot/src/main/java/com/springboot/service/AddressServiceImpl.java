package com.springboot.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.model.Address;
import com.springboot.repository.AddressRepository;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

@Service
public class AddressServiceImpl implements AddressService {

	@Autowired
	private AddressRepository addressRepository;

	@Override
	@Transactional
	public Address addAddress(Address address) {
		return addressRepository.save(address);

	}

	@Override
	@Transactional
	public Address readAddressByDoorNo(int doorNo) {
		Address address = null;
		Optional<Address> optionalAddress = addressRepository.findById(doorNo);
		if (optionalAddress.isPresent()) {
			address = optionalAddress.get();
		}

		return address;

	}

	@Override
	@Transactional
	public Address alterAddress(Address address) {

		return addressRepository.save(address);
	}

	@Override
	@Transactional
	public int removeAddress(int doorNo) {
		addressRepository.deleteById(doorNo);

		return doorNo;
	}

	@Override
	public Address readAddressByCity(String city) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Address readAddressByState(String state) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getStateById(int doorNo) {
		return addressRepository.anyName(doorNo);

	}

	@Override
	public List<Address> nativeQuery() {
		return addressRepository.getAllAddress();

	}

}
