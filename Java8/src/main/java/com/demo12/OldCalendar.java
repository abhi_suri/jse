package com.demo12;

import java.util.Calendar;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class OldCalendar {

	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		System.out.println(calendar.getTime());
		System.out.println("Current Calendar's Year : " + calendar.get(Calendar.YEAR));
		System.out.println("Current Calendar's Date : " + calendar.get(Calendar.DATE));
		System.out.println("Current Hour : " + calendar.get(Calendar.HOUR));
		System.out.println("Current Minutes : " + calendar.get(Calendar.MINUTE));
		System.out.println("Current Second : " + calendar.get(Calendar.SECOND));

	}

}
