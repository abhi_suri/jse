package com.main;

import com.exception.EmployeeException;
import com.model.Employee;
import com.service.EmployeeService;
import com.service.EmployeeServiceImpl;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class MainApp {

	public static void main(String[] args) {

		EmployeeService employeeService = new EmployeeServiceImpl();
		Employee data;
		try {
			data = employeeService.readValidateEmployeeById(12);
			if (data != null) {
				System.out.println("Employee number : " + data.getEmployeeNo());
				System.out.println("Employee name : " + data.getEmpName());
			}

		} catch (EmployeeException e) {
			System.err.println(e.getMessage());
		}

	}
}
