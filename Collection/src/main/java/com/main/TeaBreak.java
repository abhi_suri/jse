package com.main;

import java.util.HashMap;
import java.util.Map;

import com.model.Employee;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public class TeaBreak {

	public static void main(String[] args) {
		Employee employee1 = new Employee(10, "Ten", 1010.10f);
		Employee employee2 = new Employee(20, "Twenty", 2020.20f);
		Employee employee3 = new Employee(30, "Thirty", 3030.30f);
		Employee employee4 = new Employee(40, "Forty", 4040.40f);

		Map<Integer, Employee> map = new HashMap<>();
		map.put(employee1.getEmpId(), employee1);
		map.put(employee2.getEmpId(), employee2);
		map.put(employee3.getEmpId(), employee3);
		map.put(employee4.getEmpId(), employee4);

		Employee obj = map.get(10);
		System.out.println(obj.getEmpId());
		System.out.println(obj.getEmpName());

	}

}
