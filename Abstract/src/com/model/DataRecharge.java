package com.model;

/**
 *
 * @author -- Abhishek Kumar
 *
 */

public interface DataRecharge {

	public abstract String dataFor50();

	public abstract String dataFor100();

}
